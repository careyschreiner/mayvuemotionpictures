﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPICore.Models;
using WebAPICore.IServices;
using Microsoft.EntityFrameworkCore;

namespace WebAPICore.Services
{
//    public class MotionPicturesService
//    {
//    }
    public class MotionPicturesService : IMotionPicturesService
    {
        MPContext dbContext;
        public MotionPicturesService(MPContext _db)
        {
            dbContext = _db;
        }
        public IEnumerable<MotionPictures> GetMotionPictures()
        {
            var motionpictures = dbContext.MotionPictures.ToList();
            return motionpictures;
        }
        public MotionPictures AddMotionPictures(MotionPictures motionpictures)
        {
            if (motionpictures != null)
            {
                dbContext.MotionPictures.Add(motionpictures);
                dbContext.SaveChanges();
                return motionpictures;
            }
            return null;
        }
        public MotionPictures UpdateMotionPictures(MotionPictures MotionPictures)
        {
            dbContext.Entry(MotionPictures).State = EntityState.Modified;
            dbContext.SaveChanges();
            return MotionPictures;
        }
        public MotionPictures DeleteMotionPictures(int id)
        {
            var MotionPictures = dbContext.MotionPictures.FirstOrDefault(x => x.Id == id);
            dbContext.Entry(MotionPictures).State = EntityState.Deleted;
            dbContext.SaveChanges();
            return MotionPictures;
        }
        public MotionPictures GetMotionPicturesById(int id)
        {
            var MotionPictures = dbContext.MotionPictures.FirstOrDefault(x => x.Id == id);
            return MotionPictures;
        }
    }

}
