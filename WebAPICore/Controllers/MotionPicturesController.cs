﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAPICore.IServices;
using WebAPICore.Models;
using Microsoft.EntityFrameworkCore;

namespace WebAPICore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //   public class MotionPicturesController : ControllerBase
    //   {
    //   }
    public class MotionPicturesController : ControllerBase
    {
        private readonly IMotionPicturesService motionpicturesService;
        public MotionPicturesController(IMotionPicturesService motionpicture)
        {
            motionpicturesService = motionpicture;
        }
        [HttpGet]
        [Route("[action]")]
        [Route("api/MotionPictures/GetMotionPictures")]
        public IEnumerable<MotionPictures> GetMotionPictures()
        {
            return motionpicturesService.GetMotionPictures();
        }
        [HttpPost]
        [Route("[action]")]
        [Route("api/MotionPictures/AddMotionPictures")]
        public MotionPictures AddMotionPictures(MotionPictures motionpicture)
        {
            return motionpicturesService.AddMotionPictures(motionpicture);
        }
        [HttpPut]
        [Route("[action]")]
        [Route("api/MotionPictures/EditMotionPictures")]
        public MotionPictures EditMotionPictures(MotionPictures motionpicture)
        {
            return motionpicturesService.UpdateMotionPictures(motionpicture);
        }
        [HttpDelete]
        [Route("[action]")]
        [Route("api/MotionPictures/DeleteMotionPictures")]
        public MotionPictures DeleteMotionPictures(int id)
        {
            return motionpicturesService.DeleteMotionPictures(id);
        }
        [HttpGet]
        [Route("[action]")]
        [Route("api/MotionPictures/GetMotionPicturesId")]
        public MotionPictures GetMotionPicturesId(int id)
        {
            return motionpicturesService.GetMotionPicturesById(id);
        }
    }

}
