﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPICore.Models;

namespace WebAPICore.IServices
{
    public interface IMotionPicturesService
    {
        IEnumerable<MotionPictures> GetMotionPictures();

        MotionPictures GetMotionPicturesById(int id);
        MotionPictures AddMotionPictures(MotionPictures motionpictures);
        MotionPictures UpdateMotionPictures(MotionPictures motionpictures);
        MotionPictures DeleteMotionPictures(int id);
    }
}
