USE MP
GO


CREATE TABLE MotionPictures(
	[ID] [int] primary key identity(1,1),
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[ReleaseYear] [int] NOT NULL)
	GO